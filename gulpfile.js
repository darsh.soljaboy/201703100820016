
var gulp        = require('gulp');
var browserSync = require('browser-sync').create();

Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});
 
// or...

gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "localhost:8080"
});
*/
//changed code
gulp.task('browserSync', () => {
        browserSync.init({
            server: {
                baseDir: task.dir.base,
                middleware: [
                    webpackDevMiddleware(bundler, {
                        publicPath: webpackConfig.output.publicPath,
                        stats: 'errors-only'
                    })
                  ]
            },
            browser: 'chrome'
        });
    });
/*
//code
var gulp = require('gulp');
var sass = require('gulp-sass');
var bs = require('browser-sync').create();

gulp.task('browser-sync', ['sass'], function() {
    bs.init({
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('sass', function () {
    return gulp.src('scss/*.scss')
                .pipe(sass())
                .pipe(gulp.dest('css'))
                .pipe(bs.reload({stream: true}));
});

gulp.task('watch', ['browser-sync'], function () {
    gulp.watch("scss/*.scss", ['sass']);
    gulp.watch("*.html").on('change', bs.reload);
});
*/